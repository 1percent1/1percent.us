#!/usr/bin/env python3

import sys ; print(sys.version)
from datetime import date

day = date.today().strftime("%Y/%m/%d")
print(day)

try:
  import yfinance as yf
except Exception as e:
  print("please install yahoo finance module vui: pip3 install yfinance ...", e)
  sys.exit(0)

def reset_stdio():
  """
  if we need to sanitize poorly encoded text for tty output?
  """
  try:
    import codecs
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    sys.stderr = codecs.getwriter('utf-8')(sys.stderr)
  except: pass
  return

# tbd:
# import argparse, json, traceback

class Week:
  """
  Preliminary version of a date-time-range object
  """
# def __init__(self, start_end=satsunday()):
  def __init__(self, start_end=['2022-05-09', '2022-05-13']):
    """
    TBD sanity check of input dates
    """
    self.start = start_end[0] ; self.end = start_end[1]

def yf_weekly(tics, week):
  """
  yahoo finance bulk download for the week
  """
  if not(tics): tics = ['C', 'BAC', 'WFC'] ; print(tics)
  if not(week): week = Week() ; print(week)
  data = yf.download(tics, week.start, week.end, group_by="ticker")
  for t in tics:
    t = t.upper() ; print(t, 'open:', data[t]['Open'])
    t = t.upper() ; print(t, 'close:', data[t]['Close'])

  return data

def yf_options(tics):
  """
  parse options info if tics are worthy
  """
  if not(tics): return None

  optns = {} # ; print(tics)
  for t in tics:
    t = t.upper()
    q = yf.Ticker(t) # ; print(str(q))
    try:
      info = q.info.items()
      for item in info: print(t,item)
    except Exception as e: 
      print("Error printing Ticker quote for:", t, ' ...', e)

    try:
      opts_exp = q.options
      exp = opts_exp[0]
      opt = q.option_chain(exp)
      # print(exp, opt.calls, opt.puts)
      optns[t] = { exp, str(info), str(opt.calls), str(opt.puts) }
    except Exception as e: 
      print("Error fetching Options Chain for:", t, ' ...', e)

  # hopefully at least 1 option chain has been fetched
  return optns

def print_weekly_prices(tics, week):
  """
  tbd prettyprint
  """
  prices = yf_weekly(tics, week)
  for t in tics:
    print(t, prices[t])

def print_options(tics):
  """
  tbd prettyprint
  """
  optns = yf_options(tics)
  for t in tics:
    print(t, optns)

def satsunday(input=date.today()):
  """
  weekend day-date check. yahoo finance may not be available?
  """
  d = input.toordinal()
  last = d - 6
  sunday = last - (last % 7) ; sund = date.fromordinal(sunday)
  saturday = sunday + 6 ; satd = date.fromordinal(saturday)
  return [str(sund), str(satd)]


def main(args):
  """
  args string provides tickers of interest
  """
  #tics = "atax diax dsl fof gut hqh hql jta jtd mfd ohi bdcs sdiv slv tpz wfc yyy"
  tics = "BAC WFC C" # big banks
  if(args):
    tics = str(args)
  tics = tics.upper().split(' ') ; print(tics)
  ssday = satsunday()
  data = yf_weekly(tics, Week(ssday))
  options_dict = yf_options(tics)

if __name__ == '__main__':
  args = None
  if len(sys.argv) > 1: args = sys.argv[1:]
  main(args)

